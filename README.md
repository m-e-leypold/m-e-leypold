PSA: Migrating to Codeberg imminent
===================================

Repositories in my Gitlab account will be migrated to
[Codeberg](https://codeberg.org/m-e-leypold) in the coming months
(May - June 2024). All my development on Gitlab will cease. Finally
the repositories will be deleted.

Rationale: I take offense at Gitlab's stance with respect to the
copyright of contributors to the allmende. As opposed to Github they
never seem to admit publicly that they train their AI products on the
repositories they host, but a second glance reveals:

- Gitlab uses the Anthropic Claude model for powering its AI services, see
  [about.gitlab.com/blog/2024/01/16/gitlab-uses-anthropic-for-smart-safe-ai-assisted-code-generation/](https://about.gitlab.com/blog/2024/01/16/gitlab-uses-anthropic-for-smart-safe-ai-assisted-code-generation/)

- Anthropic is not transparent about the training data that went into
  Claude. So we can just assume (given the general gold rush
  mentality encountered currently in the so-called "AI" industry) that
  the training data has been scraped without scruples from publicly
  readable information without giving a hoot for licenses and
  copyright.

I consider this unethical.

Personally I hold the opinion that trained LLMs / neural networks /
whatever are derived works of the training material they ingest. 

I will (it seems) have a hard time enforcing the GPL based on my
reading, but this doesn't make the act of appropriating the work of
creators intended for the allmende less morally reprehensible. Might
does not make right.

I am done with Gitlab (and Github, for the same reason).

